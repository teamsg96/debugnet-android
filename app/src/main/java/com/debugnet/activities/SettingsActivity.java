package com.debugnet.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.debugnet.R;
import com.debugnet.activities.common.DialogGenerator;
import com.debugnet.activities.common.GoToMainMenuCallback;

public class SettingsActivity extends AppCompatActivity {
    private EditText downloadTextBox;
    private EditText uploadTextBox;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        downloadTextBox = findViewById(R.id.downloadTextBox);
        uploadTextBox = findViewById(R.id.uploadTextBox);

        // Load settings for preferences
        sharedPref = getSharedPreferences("settings", MODE_PRIVATE);

        // Load all previously set settings
        if (sharedPref.contains("promisedDownloadSpeed")) {
            downloadTextBox.setText(String.valueOf(sharedPref.getFloat("promisedDownloadSpeed", -1)));
        }
        if (sharedPref.contains("promisedUploadSpeed")) {
            uploadTextBox.setText(String.valueOf(sharedPref.getFloat("promisedUploadSpeed", -1)));
        }

    }

    /*
    Update settings when save is pressed
     */
    public void onSaveClick(View v){
        SharedPreferences.Editor settingsEditor = sharedPref.edit();

        // Update any settings provided
        if (!downloadTextBox.getText().toString().isEmpty()) {
            settingsEditor.putFloat("promisedDownloadSpeed", Float.valueOf(downloadTextBox.getText().toString()));
        }

        if (!uploadTextBox.getText().toString().isEmpty()) {
            settingsEditor.putFloat("promisedUploadSpeed", Float.valueOf(uploadTextBox.getText().toString()));
        }

        settingsEditor.commit();

        // Inform user settings have been updated
        DialogGenerator.createInfoBoxWithOkButton(
                "Settings updated!",
                "",
                new GoToMainMenuCallback(this),
                this
        );
    }
}
