package com.debugnet.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.debugnet.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onDevicesClick(View v){
        startActivity(new Intent(MainActivity.this,DevicesActivity.class));
    }

    public void onTroubleshootClick(View v){
        startActivity(new Intent(MainActivity.this,SelectProblemActivity.class));
    }

    public void onFactClick(View v){
        startActivity(new Intent(MainActivity.this,SelectFactActivity.class));
    }

    public void onSettingsClick(View v){
        startActivity(new Intent(MainActivity.this,SettingsActivity.class));
    }
}
