package com.debugnet.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import rest.debugnet_rest_service.DebugnetAPI;
import rest.debugnet_rest_service.objs.Problem;
import rest.debugnet_rest_service.objs.TroubleshootResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.debugnet.R;
import com.debugnet.activities.common.DialogGenerator;
import com.debugnet.activities.common.GoToMainMenuCallback;

import java.util.ArrayList;

public class TroubleshootReportActivity extends AppCompatActivity {
    private ConstraintLayout reportLayout;
    private TextView reportQueryNameBox;
    private TextView reportProblemAndAdviceBox;

    private ArrayList<Problem> allProblems;
    private int currentProblemIndx = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troubleshoot_report);

        // Make both text view boxes scrollable
        reportQueryNameBox = findViewById(R.id.reportQueryNameBox);
        reportQueryNameBox.setMovementMethod(new ScrollingMovementMethod());
        reportProblemAndAdviceBox = findViewById(R.id.reportProblemAndAdviceBox);
        reportProblemAndAdviceBox.setMovementMethod(new ScrollingMovementMethod());

        // Hide all content for now until the troubleshoot report has been returned
        reportLayout = findViewById(R.id.reportLayout);
        reportLayout.setVisibility(View.INVISIBLE);

        // Display a popup to tell the user that a diagnostic is running and that they should wait
        // until it is completed.
        AlertDialog loadingPopup = DialogGenerator.createInfoBoxNoButton(
                "Running diagnostics on your network",
                "Please wait until diagnostics have completed...",
                this);

        // Execute troubleshoot request and listen out for a response with a callback
        new TroubleshootHandler(this, loadingPopup).executeTroubleshoot(
                (DebugnetAPI.TROUBLESHOOT_QUERY) getIntent().getSerializableExtra("queryId"));
    }

    /*
    Displays the next discovered problem with its advice inside the text view
     */
    public void displayNextProblem(){
        String boxText;

        currentProblemIndx++;

        boxText = allProblems.get(currentProblemIndx).getProblemDescription();
        boxText += "<br/><br/><b>Advice:</b>";
        boxText += allProblems.get(currentProblemIndx).getAdvice();

        reportProblemAndAdviceBox.setText(Html.fromHtml(boxText));
        // Reset scroll view to top
        reportProblemAndAdviceBox.scrollTo(0,0);

    }

    /*
    Handle when the no button is pressed
     */
    public void onNoClick(View v){
        // Display next problem in text view, if their is another to show. Otherwise display a
        // message informing the user that its the end of the troubleshoot.
        if (currentProblemIndx + 1 < allProblems.size()){
            displayNextProblem();
        }
        else{
            DialogGenerator.createInfoBoxWithOkButton(
                "End of troubleshoot",
                "Try contacting your ISP and device manufacturer",
                    new GoToMainMenuCallback(TroubleshootReportActivity.this),
                this
            );
        }
    }

    /*
    Go to the main menu if the user is finished with the troubleshoot
     */
    public void onYesClick(View v){
        startActivity(new Intent(this, MainActivity.class));
    }

    public void setAllProblems(ArrayList<Problem> allProblems) {
        this.allProblems = allProblems;
    }
}

/*
Handles the execution of the troubleshoot for frontend
 */
class TroubleshootHandler implements Callback<TroubleshootResponse>{
    private AlertDialog loadingPopup;
    private TroubleshootReportActivity tsActivity;

    public TroubleshootHandler(TroubleshootReportActivity tsActivity, AlertDialog loadingPopup) {
        this.tsActivity = tsActivity;
        this.loadingPopup = loadingPopup;
    }

    /*
    Execute the desired troubleshoot query and execute it with the required arguments
     */
    public void executeTroubleshoot(DebugnetAPI.TROUBLESHOOT_QUERY queryId){
        DebugnetAPI dbNetApi = new DebugnetAPI();
        dbNetApi.setup();

        Intent intent = tsActivity.getIntent();

        switch (queryId) {
            case TROUBLESHOOT_SLOW_DEVICE:
                dbNetApi.runSlowDeviceTroubleshoot(
                        intent.getStringExtra("deviceMac"),
                        intent.getIntExtra("duration", -1),
                        this);
                break;

            case TROUBLESHOOT_CONN_DROP:
                dbNetApi.runRegularConnDropDeviceTroubleshoot(
                        intent.getStringExtra("deviceMac"),
                        intent.getIntExtra("duration", -1),
                        this);
                break;

            case TROUBLESHOOT_SLOW_NETWORK:
                dbNetApi.runSlowNetworkTroubleshoot(
                         intent.getIntExtra("duration", -1),
                        this);
                break;
        }
    }

    /*
    Handle the response from a troubleshoot request
     */
    public void onResponse(Call<TroubleshootResponse> call, Response<TroubleshootResponse> response) {
        Log.i("REST", "Code: " + String.valueOf(response.code()));

        loadingPopup.cancel();

        if (!response.isSuccessful()){
            DialogGenerator.createErrorBoxForCommunicationError(
                    new GoToMainMenuCallback(tsActivity),
                    tsActivity
            );
            return;
        }

        // Store all problems found in the activity
        ArrayList<Problem> allProblems = response.body().getAllProblems().getAllProblemsInSeverityOrder();
        tsActivity.setAllProblems(allProblems);

        // Display a dialog box if no problems where found and display an 'ok' which will go to the
        // main menu.
        if (allProblems.isEmpty()){
            DialogGenerator.createInfoBoxWithOkButton(
                    "No problems found!",
                    ("DebugNet was unable to find anything wrong that could be causing " +
                            "your issue."),
                    new GoToMainMenuCallback(tsActivity),
                    tsActivity
            );
            return;
        }

        // Problems where found so make the report window visible
        tsActivity.findViewById(R.id.reportLayout).setVisibility(View.VISIBLE);

        // Display the query name
        TextView reportQueryNameBox = tsActivity.findViewById(R.id.reportQueryNameBox);
        reportQueryNameBox.setText(response.body().getQueryName());

        // Display the first problem
        tsActivity.displayNextProblem();

        Log.i("REST", response.body().getQueryName());
    }

    /*
    Handle when the REST call fails.
     */
    public void onFailure(Call<TroubleshootResponse> call, Throwable throwable) {
        DialogGenerator.createErrorBoxForNoConnection(
                new GoToMainMenuCallback(tsActivity),
                tsActivity
        );

        Log.e("REST", throwable.toString());
    }
}