package com.debugnet.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import rest.debugnet_rest_service.DebugnetAPI;
import rest.debugnet_rest_service.DebugnetAPI.TROUBLESHOOT_QUERY;
import rest.debugnet_rest_service.objs.Device;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;

import com.debugnet.R;
import com.debugnet.activities.common.DialogGenerator;
import com.debugnet.activities.common.GoToMainMenuCallback;

import java.util.ArrayList;

public class AdditionalTsOptsActivity extends AppCompatActivity {
    private ArrayList<Device> allDevices;
    private final int dropDownHeight = 140;
    private final int marginBetweenParamFields = 150;
    private final int marginBetweenParamNameAndEntry = 15;
    private final int marginSides = 0;
    private final int marginToLayoutTop = 20;
    private TROUBLESHOOT_QUERY queryId;
    private LinearLayout additionalParamsLayout;
    private Spinner durationSpinner;
    private Spinner devicesSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_ts_opts);

        additionalParamsLayout = findViewById(R.id.additionalParamsLayout);

        queryId = (TROUBLESHOOT_QUERY) getIntent().getSerializableExtra("queryId");

        // Some troubleshoot queries will require additional parameters to be defined by the user.
        // The exact parameters required is specific to the query. The switch box creates the
        // necessary input fields to provide the additional parameters.
        switch (queryId) {
            case TROUBLESHOOT_SLOW_DEVICE:
                // The target device and duration of problem is required.
                createDurationDropDown(marginToLayoutTop);
                createDevicesDropDown(marginBetweenParamFields);
                break;

            case TROUBLESHOOT_CONN_DROP:
                // The target device and duration of problem is required.
                createDurationDropDown(marginToLayoutTop);
                createDevicesDropDown(marginBetweenParamFields);
                break;

            case TROUBLESHOOT_SLOW_NETWORK:
                // The duration of problem is required.
                createDurationDropDown(marginToLayoutTop);
                break;
        }
    }

    /*
    Creates a dropdown box to allow input of a issue duration
    */
    private void createDevicesDropDown(int topMargin){
        final int titleWidth = 920;
        final int titleHeight = 80;

        // Insert devices image title
        ImageView devicesTitle = new ImageView(this);
        devicesTitle.setBackgroundResource(R.drawable.selectdevicetext);
        LayoutParams devicesTitleParams = new LayoutParams(titleWidth, titleHeight);
        devicesTitleParams.setMargins(marginSides, topMargin, marginSides, marginBetweenParamNameAndEntry);
        additionalParamsLayout.addView(devicesTitle, devicesTitleParams);

        // Create an empty spinner box for the list of devices.
        devicesSpinner = new Spinner(this);
        devicesSpinner.setBackgroundResource(R.drawable.dropdown);
        LayoutParams spinnerParams = new LayoutParams(LayoutParams.MATCH_PARENT, dropDownHeight);
        additionalParamsLayout.addView(devicesSpinner, spinnerParams);

        // Get all devices from the router. Whilst operation is in progress a dialog popup instructs
        // the user to wait whilst the process is ongoing. A callback handles the response from the
        // REST API.
        AlertDialog waitingForRouterPopup = DialogGenerator.createInfoBoxForCommunicationWait(this);
        DebugnetAPI dbNetApi = new DebugnetAPI();
        dbNetApi.setup();
        dbNetApi.getDevices(new GetAllDevicesOptsCallback(this, waitingForRouterPopup, devicesSpinner));
    }

    /*
    Creates a dropdown box to allow input of a issue duration
     */
    private void createDurationDropDown(int topMargin){
        final int titleWidth = 480;
        final int titleHeight = 80;

        // Insert duration image title
        ImageView durationTitle = new ImageView(this);
        durationTitle.setBackgroundResource(R.drawable.problemdurationtext);
        LayoutParams durationTitleParams = new LayoutParams(titleWidth, titleHeight);
        durationTitleParams.setMargins(marginSides, topMargin, marginSides, marginBetweenParamNameAndEntry);
        additionalParamsLayout.addView(durationTitle, durationTitleParams);

        // Insert spinner field to allow the user to select duration from a list of duration options
        durationSpinner = new Spinner(this);
        durationSpinner.setBackgroundResource(R.drawable.dropdown);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.time_opts, R.layout.dbnet_text);
        adapter.setDropDownViewResource(R.layout.dbnet_text);
        durationSpinner.setAdapter(adapter);
        LayoutParams spinnerParams = new LayoutParams(LayoutParams.MATCH_PARENT, dropDownHeight);
        additionalParamsLayout.addView(durationSpinner, spinnerParams);
    }

    /*
    Fetch user input and proceed onto next activity
     */
    public void onRunClick(View v){
        Intent intent = new Intent(AdditionalTsOptsActivity.this,
                TroubleshootReportActivity.class);

        // Get duration if a duration spinner was used
        if (durationSpinner != null){
            int seconds = getSelectedDurationInSeconds();

            // Show an error message and don't proceed if their was issues processing the duration
            // period.
            if (seconds == -1){
                DialogGenerator.createInfoBoxOverlaid(
                        "DebugNet error",
                        "Something has gone wrong processing the 'duration'",
                        AdditionalTsOptsActivity.this);

                return;
            }

            intent.putExtra("duration", seconds);
        }

        // Get device if a device spinner was used
        if (devicesSpinner != null){
            Device selectedDevice = allDevices.get(devicesSpinner.getSelectedItemPosition());
            // URI strings don't like colons in string parameters, so they are removed. The REST
            // server expects their to be no colons.
            intent.putExtra("deviceMac", selectedDevice.getMacNoColons());
        }

        intent.putExtra("queryId", queryId);
        startActivity(intent);
    }

    /*
    Get the number of seconds for the time period selected
     */
    private int getSelectedDurationInSeconds(){
        int seconds;
        String timeFrameStr = durationSpinner.getSelectedItem().toString();

        Log.i("durationTimeBox", "selected -> " + timeFrameStr);

        // Break out the unit of time and the value for the unit of time
        String timeUnit = timeFrameStr.split("\\s")[1];
        int timeValue = Integer.valueOf(timeFrameStr.split("\\s")[0]);

        // Convert selected time period to seconds
        if (timeUnit.equals("minute") || timeUnit.equals("minutes")){
            // Multiple number of minutes by number of seconds in a minute
            seconds = timeValue * 60;
        }
        else if (timeUnit.equals("hour") || timeUnit.equals("hours")){
            // Multiple number of hours by number of seconds in a hour
            seconds = timeValue * 3600;
        }
        else if (timeUnit.equals("day") || timeUnit.equals("days")){
            // Multiple number of days by number of seconds in a day
            seconds = timeValue * 86400;
        }
        else if (timeUnit.equals("week") || timeUnit.equals("weeks")){
            // Multiple number of weeks by number of seconds in a week
            seconds = timeValue * 604800;
        }
        else if (timeUnit.equals("month") || timeUnit.equals("months")){
            // Multiple number of months by number of seconds in a month
            seconds = timeValue * 2592000;
        }
        else{
            // Mark conversion as failed
            seconds = -1;
        }

        Log.i("durationTimeBox", "convertToSeconds -> " + String.valueOf(seconds));

        return seconds;
    }

    public void setAllDevices(ArrayList<Device> allDevices) {
        this.allDevices = allDevices;
    }
}

/*
Callback to handle the response from calling 'GET devices/'
 */
class GetAllDevicesOptsCallback implements Callback<ArrayList<Device>> {
    private AdditionalTsOptsActivity activity;
    private AlertDialog waitingForRouterPopup;
    private Spinner devicesSpinner;

    GetAllDevicesOptsCallback(AdditionalTsOptsActivity activity,
                              AlertDialog waitingForRouterPopup,
                              Spinner deviceSpinner) {
        this.activity = activity;
        this.waitingForRouterPopup = waitingForRouterPopup;
        this.devicesSpinner = deviceSpinner;
    }

    /*
    Handle the response
     */
    public void onResponse(Call<ArrayList<Device>> call, Response<ArrayList<Device>> response) {
        Log.i("REST", "Code: " + String.valueOf(response.code()));

        if (!response.isSuccessful()){
            DialogGenerator.createErrorBoxForCommunicationError(
                    new GoToMainMenuCallback(activity),
                    activity
            );
            return;
        }

        // Display a dialog box if no devices where found and display an 'ok' button which will go
        // to the main menu.
        if (response.body().isEmpty()){
            DialogGenerator.createInfoBoxWithOkButton(
                    "No devices found!",
                    ("DebugNet was unable to find any devices connected to the router. If devices " +
                            "are actually in fact connected, then try restarting the router."),
                    new GoToMainMenuCallback(activity),
                    activity
            );
            return;
        }

        activity.setAllDevices(response.body());

        // Extract the human readable name for each device
        ArrayList<String> deviceHumanNames = new ArrayList();
        for (Device device : response.body()){
            deviceHumanNames.add(device.getHumanName());
        }

        // Use the human readable name for each device in the spinner box.
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, R.layout.dbnet_text, deviceHumanNames);
        devicesSpinner.setAdapter(adapter);

        // Close the 'communicating with router' box as the operation is now complete
        waitingForRouterPopup.cancel();

        Log.i("REST", "Devices found: " + String.valueOf(response.body().size()));
    }

    /*
    Handle when the REST call fails.
     */
    public void onFailure(Call<ArrayList<Device>> call, Throwable throwable) {
        DialogGenerator.createErrorBoxForNoConnection(
                new GoToMainMenuCallback(activity),
                activity
        );

        Log.e("REST", throwable.toString());
    }
}