package com.debugnet.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import rest.debugnet_rest_service.DebugnetAPI;
import rest.debugnet_rest_service.objs.Device;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.debugnet.R;
import com.debugnet.activities.common.DialogGenerator;
import com.debugnet.activities.common.GoToMainMenuCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class DevicesActivity extends AppCompatActivity {
    DebugnetAPI dbNetApi;
    private DeviceTiles deviceTiles;

    private final int SECONDS_TO_COLLECT_METRICS = 600; // 10 minutes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        // Create the object that manages the device tiles on the window
        deviceTiles = new DeviceTiles(this, (LinearLayout)findViewById(R.id.devicesLayout));

        // Setup spinner to offer sort options
        Spinner sortOptsDropDown = findViewById(R.id.sortSpinner);
        sortOptsDropDown.setOnItemSelectedListener(new SortSpinnerListener(deviceTiles));
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.device_sort_opts, R.layout.dbnet_text);
        adapter.setDropDownViewResource(R.layout.dbnet_text);
        sortOptsDropDown.setAdapter(adapter);

        // Create a popup that tells the user to wait for server communication
        AlertDialog waitingForRouterPopup = DialogGenerator.createInfoBoxForCommunicationWait(this);

        // Get the latest information on the networked devices, with basic metric data
        dbNetApi = new DebugnetAPI();
        dbNetApi.setup();
        dbNetApi.getDevicesWithMetrics(SECONDS_TO_COLLECT_METRICS,
                new GetAllDevicesCallback(this, waitingForRouterPopup, deviceTiles));
    }

    /*
    Handle when the refresh button is pressed
     */
    public void onRefreshClick(View view){
        // Create a popup that tells the user to wait for server communication
        AlertDialog waitingForRouterPopup = DialogGenerator.createInfoBoxForCommunicationWait(this);

        dbNetApi.getDevicesWithMetrics(SECONDS_TO_COLLECT_METRICS,
                new GetAllDevicesCallback(this, waitingForRouterPopup, deviceTiles));
    }
}

/*
Manages device tiles on the view
 */
class DeviceTiles{
    private AppCompatActivity activity;
    private LinearLayout allTilesView;

    private ArrayList<Device> devices;

    // Tile design parameters
    private int ATTRIB_CELL_HEIGHT = 70;
    private int ATTRIB_CELL_WIDTH = 500;
    private int TILE_TOP_HEIGHT = 120;
    private int TILE_BOTTOM_HEIGHT = 80;

    private int ATTRIB_LABEL_TEXT_SIZE = 16;
    private int ATTRIB_VALUE_TEXT_SIZE = 15;
    private int DEVICE_NAME_TEXT_SIZE = 35;
    private int STATUS_TEXT_SIZE = 26;

    private int ATTRIB_CELL_PADDING_LEFT = 20;
    private int ATTRIB_LABEL_WEIGHT = 1;
    private int ATTRIB_VALUE_WEIGHT = 2;

    // Signal performance indicators
    private int SIGNAL_GOOD = 70;
    private int SIGNAL_BAD = 80;


    public DeviceTiles(AppCompatActivity activity, LinearLayout allTilesView) {
        this.activity = activity;
        this.allTilesView = allTilesView;
        this.devices = new ArrayList<>();
    }

    /*
    Create a row for a device attribute with the default value color
     */
    private void addAttribRow(String label, String value, TableLayout attribTable){
        addAttribRow(label, value, Color.WHITE, attribTable);
    }

    /*
    Create a row for a device attribute
     */
    private void addAttribRow(String label, String value, int valueColor, TableLayout attribTable){
        TableRow newRow = new TableRow(activity);

        TextView labelTV = createAttribLabelView(label);
        TextView valueTV = createAttribValueView(value);
        valueTV.setTextColor(valueColor);

        LinearLayout.LayoutParams attribLabelLayout = new TableRow.LayoutParams(
                ATTRIB_CELL_WIDTH, ATTRIB_CELL_HEIGHT);
        // attribLabelLayout.weight=ATTRIB_LABEL_WEIGHT;
        LinearLayout.LayoutParams attribValueLayout = new TableRow.LayoutParams(
                0, ATTRIB_CELL_HEIGHT);
        attribValueLayout.weight=ATTRIB_VALUE_WEIGHT;

        newRow.addView(labelTV, attribLabelLayout);
        newRow.addView(valueTV, attribValueLayout);

        attribTable.addView(newRow);
    }

    /*
    Create a text view for an attribute value
     */
    private TextView createAttribValueView(String value){
        TextView attribValue = new TextView(activity);
        attribValue.setTextColor(Color.WHITE);
        attribValue.setText(value);
        attribValue.setTextSize(ATTRIB_VALUE_TEXT_SIZE);
        attribValue.setBackgroundResource(R.drawable.tilebox);
        attribValue.setGravity(Gravity.CENTER_VERTICAL);
        attribValue.setPadding(ATTRIB_CELL_PADDING_LEFT,0,0,0);

        return attribValue;
    }

    /*
    Create a text view for an attribute label
     */
    private TextView createAttribLabelView(String label){
        TextView attribLabel = new TextView(activity);
        attribLabel.setTextColor(Color.WHITE);
        attribLabel.setText(label);
        attribLabel.setTextSize(ATTRIB_LABEL_TEXT_SIZE);
        attribLabel.setTypeface(attribLabel.getTypeface(), Typeface.BOLD);
        attribLabel.setBackgroundResource(R.drawable.tilebox);
        attribLabel.setGravity(Gravity.CENTER_VERTICAL);
        attribLabel.setPadding(ATTRIB_CELL_PADDING_LEFT,0,0,0);

        return attribLabel;
    }

    /*
    Create the body of the tile that contains device attributes
     */
    private TableLayout createTileBody(Device device){
        TableLayout attribTable;
        attribTable = new TableLayout(activity);
        attribTable.setBackgroundColor(activity.getResources().getColor(R.color.debugnetBlue));

        // Format data attributes
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z");
        // Convert Unix seconds to milliseconds
        String firstConn = sdf.format(new Date(device.getFirstConnected()*1000L));
        String lastConn = sdf.format(new Date(device.getLastConnected()*1000L));

        // Provide information on the WifI connectivity of the device
        String wifiStr;
        if (device.isWireless()){
            if (device.getConnType() == Device.CONN_TYPE.CONN_WIFI_2_4){
                wifiStr = "Connected using 2.4Ghz";
            }
            else{
                wifiStr = "Connected using 5Ghz";
            }
        }
        else{
            wifiStr = "-";
        }

        // Format speed values
        String downloadSpeedStr = String.format("%.2f", device.getDownloadSpeed()) + " Mbps";
        String uploadSpeedStr = String.format("%.2f", device.getUploadSpeed()) + " Mbps";

        // Add device attributes
        addAttribRow("Hostname", device.getHostname(), attribTable);
        addAttribRow("IP Address", device.getIpAddress(), attribTable);
        addAttribRow("MAC Address", device.getMac(), attribTable);
        addAttribRow("Manufacturer", device.getManufacturer(), attribTable);
        addAttribRow("Last connected", lastConn, attribTable);
        addAttribRow("First connected", firstConn, attribTable);
        addAttribRow("Average download speed used", downloadSpeedStr, attribTable);
        addAttribRow("Average upload speed used", uploadSpeedStr, attribTable);
        addAttribRow("Router WiFi", wifiStr, attribTable);

        // Describe the signal quality if the device is connected to the router on WiFi
        if (device.isWireless()){
            String sigQuality;
            int valColor;

            Log.i("SIGNAL STRENGTH:", String.valueOf(device.getSignalStrength()));

            if (device.getSignalStrength() <= 0){
                sigQuality = "-";
                valColor = Color.WHITE;
            }
            else if (device.getSignalStrength() < SIGNAL_GOOD){
                sigQuality = "Good";
                valColor = Color.GREEN;
            }
            else if (device.getSignalStrength() >= SIGNAL_GOOD && device.getSignalStrength() < SIGNAL_BAD){
                sigQuality = "Fair";
                valColor = Color.YELLOW;
            }
            else{
                sigQuality = "Poor";
                valColor = Color.RED;
            }

            addAttribRow("WiFi signal strength", sigQuality, valColor, attribTable);
        }

        return attribTable;
    }

    /*
    Create the top part of the tile which contains the device name
     */
    private TextView createTileTop(String deviceName){
        TextView tileTitleView = new TextView(activity);
        tileTitleView.setText(deviceName);
        tileTitleView.setTextSize(DEVICE_NAME_TEXT_SIZE);
        tileTitleView.setTextColor(Color.WHITE);
        tileTitleView.setTypeface(tileTitleView.getTypeface(), Typeface.BOLD);
        tileTitleView.setBackgroundResource(R.drawable.tiletop);
        tileTitleView.setGravity(Gravity.CENTER_HORIZONTAL);

        return tileTitleView;
    }

    /*
    Create the bottom section of the tile that contains the status of the connection
     */
    private TextView createTileBottom(Device device){
        TextView statusTextView = new TextView(activity);
        statusTextView.setText(device.getStatusStr());
        statusTextView.setTextSize(STATUS_TEXT_SIZE);

        // Make the status text colour relative to the actual status
        if (device.getStatus() == Device.DEV_STATUS.DEV_ONLINE) {
            statusTextView.setTextColor(Color.GREEN);
        }
        else{
            statusTextView.setTextColor(Color.RED);
        }
        statusTextView.setTypeface(statusTextView.getTypeface(), Typeface.BOLD);
        statusTextView.setBackgroundResource(R.drawable.tilebottom);
        statusTextView.setGravity(Gravity.CENTER_HORIZONTAL);

        return statusTextView;
    }

    /*
    Get the layout parameters for a tile
     */
    private LinearLayout.LayoutParams getTileLayoutParams(){
        LinearLayout.LayoutParams tileLayout = new TableRow.LayoutParams();
        tileLayout.setMargins(0,15,0,15);

        return tileLayout;
    }

    /*
    Store devices
     */
    public void setDevices(ArrayList<Device> devices){
        this.devices = devices;
    }

    /*
    Create and load a tile for a device into view
     */
    public void addTile(Device device){
        // Create view to hold the tile
        LinearLayout tile = new LinearLayout(activity);
        tile.setOrientation(LinearLayout.VERTICAL);


        // Create the top part of the tile that holds the device name
        TextView devNameTextView = createTileTop(device.getHumanName());
        LinearLayout.LayoutParams devNameLayout = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, TILE_TOP_HEIGHT);
        tile.addView(devNameTextView, devNameLayout);

        // Load device attributes into the tile
        TableLayout attribTable = createTileBody(device);
        TableRow.LayoutParams attribTableLayout = new TableRow.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        tile.addView(attribTable, attribTableLayout);

        // Create the bottom part of the tile which has the status of the connection
        TextView statusTextView = createTileBottom(device);
        LinearLayout.LayoutParams statusLayout = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, TILE_BOTTOM_HEIGHT);
        tile.addView(statusTextView, statusLayout);

        // Add tile to view
        allTilesView.addView(tile, getTileLayoutParams());
    }

    /*
    Clear all tiles from view and re-add the,
     */
    public void reInsertTiles(){
        // Clear all tiles from view
        allTilesView.removeAllViews();

        // Re add tile for every device
        for (Device device : devices){
            addTile(device);
        }
    }

    /*
    Reorder the tiles by device name
    */
    public void sortTilesByDeviceName(final boolean reversed){
        // Sort devices by device name
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                if (!reversed) {
                    return d1.getHumanName().compareToIgnoreCase(d2.getHumanName());
                }
                else{
                    return d2.getHumanName().compareToIgnoreCase(d1.getHumanName());
                }
            }

        });

        reInsertTiles();
    }

    /*
    Reorder the tiles by download speed
     */
    public void sortTilesByDownloadSpeed(final boolean reversed){
        // Sort devices by download speed
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                int result;

                if (d1.getDownloadSpeed() == d2.getDownloadSpeed()){
                    result = 0;
                }
                else if(d1.getDownloadSpeed() > d2.getDownloadSpeed()){
                    result = -1;
                }
                else{
                    result = 1;
                }

                if (reversed){
                    // Flip result if order is reversed
                    result *= -1;
                }

                return result;
            }

        });

        reInsertTiles();
    }

    /*
    Reorder the tiles by upload speed
    */
    public void sortTilesByUploadSpeed(final boolean reversed){
        // Sort devices by download speed
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                int result;

                if (d1.getUploadSpeed() == d2.getUploadSpeed()){
                    result = 0;
                }
                else if(d1.getUploadSpeed() > d2.getUploadSpeed()){
                    result = -1;
                }
                else{
                    result = 1;
                }

                if (reversed){
                    // Flip result if order is reversed
                    result *= -1;
                }

                return result;

            }

        });

        reInsertTiles();
    }

    /*
    Reorder the tiles by signal strength
    */
    public void sortTilesBySignalStrength(final boolean reversed){
        // Sort devices by signal strength
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                if (d1.getSignalStrength() == d2.getSignalStrength()){
                    return 0;
                }
                else if (d1.getSignalStrength() <= 0){
                    // d1 has no signal value
                    return 1;
                }
                else if (d2.getSignalStrength() <= 0){
                    // d2 has no signal value
                    return -1;
                }
                else if (!reversed){
                    return (int)(d1.getSignalStrength() - d2.getSignalStrength());
                }
                else{
                    return (int)(d2.getSignalStrength() - d1.getSignalStrength());
                }
            }

        });

        reInsertTiles();
    }

    /*
    Reorder the tiles by first connected time/date
    */
    public void sortTilesByFirstConnected(){
        // Sort devices by first connected time and date
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                if (d1.getFirstConnected() == d2.getFirstConnected()){
                    return 0;
                }
                else if(d1.getFirstConnected() > d2.getFirstConnected()){
                    return -1;
                }
                else{
                    return 1;
                }
            }

        });

        reInsertTiles();
    }

    /*
    Reorder the tiles by last connected time/date
    */
    public void sortTilesByLastConnected(){
        // Sort devices by last connected time and date
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                if (d1.getLastConnected() == d2.getLastConnected()){
                    return 0;
                }
                else if(d1.getLastConnected() > d2.getLastConnected()){
                    return -1;
                }
                else{
                    return 1;
                }
            }

        });

        reInsertTiles();
    }

    /*
    Reorder the tiles by status
    */
    public void sortByStatus(final boolean reverse){
        // Sort devices by status
        Collections.sort(devices, new Comparator<Device>() {
            @Override
            public int compare(Device d1, Device d2) {
                if (!reverse) {
                    return d1.getStatus().compareTo(d2.getStatus());
                }
                else{
                    return d2.getStatus().compareTo(d1.getStatus());
                }
            }

        });

        reInsertTiles();
    }
}

/*
Callback to handle the response from calling 'GET devices/'
 */
class GetAllDevicesCallback implements Callback<ArrayList<Device>> {
    private AlertDialog waitingForRouterPopup;
    private DevicesActivity activity;
    private DeviceTiles deviceTiles;

    public GetAllDevicesCallback(DevicesActivity activity,
                                 AlertDialog waitingForRouterPopup,
                                 DeviceTiles deviceTiles) {
        this.activity = activity;
        this.waitingForRouterPopup = waitingForRouterPopup;
        this.deviceTiles = deviceTiles;
    }

    /*
    Handle the response
     */
    public void onResponse(Call<ArrayList<Device>> call, Response<ArrayList<Device>> response) {
        Log.i("REST", "Code: " + String.valueOf(response.code()));

        if (!response.isSuccessful()){
            DialogGenerator.createErrorBoxForCommunicationError(
                    new GoToMainMenuCallback(activity),
                    activity
            );
            return;
        }

        // Display a dialog box if no devices where found and display an 'ok' button which will go
        // to the main menu.
        if (response.body().isEmpty()){
            DialogGenerator.createInfoBoxWithOkButton(
                    "No devices found!",
                    ("DebugNet was unable to find any devices connected to the router. If devices " +
                            "are actually in fact connected, then try restarting the router."),
                    new GoToMainMenuCallback(activity),
                    activity
            );
            return;
        }

        // Store devices default to displaying tiles in order of status significance
        deviceTiles.setDevices(response.body());
        deviceTiles.sortByStatus(false);

        // Close the 'communicating with router' box as the operation is now complete
        waitingForRouterPopup.cancel();

        Log.i("REST", "Devices found: " + String.valueOf(response.body().size()));
    }

    /*
    Handle when the REST call fails.
     */
    public void onFailure(Call<ArrayList<Device>> call, Throwable throwable) {
        DialogGenerator.createErrorBoxForNoConnection(
                new GoToMainMenuCallback(activity),
                activity
        );

        Log.e("REST", throwable.toString());
    }
}

/*
Listens for option selections on the sort spinner
 */
class SortSpinnerListener implements AdapterView.OnItemSelectedListener{
    DeviceTiles deviceTiles;

    public SortSpinnerListener(DeviceTiles deviceTiles) {
        this.deviceTiles = deviceTiles;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedSortName = parent.getItemAtPosition(position).toString();

        // Sort device tiles by selected option
        if (selectedSortName.equals("Status")){
            deviceTiles.sortByStatus(false);
        }
        else if (selectedSortName.equals("Status (reversed)")){
            deviceTiles.sortByStatus(true);
        }
        else if (selectedSortName.equals("Name")){
            deviceTiles.sortTilesByDeviceName(false);
        }
        else if (selectedSortName.equals("Name (reversed)")){
            deviceTiles.sortTilesByDeviceName(true);
        }
        else if (selectedSortName.equals("Download speed (high to low)")){
            deviceTiles.sortTilesByDownloadSpeed(false);
        }
        else if (selectedSortName.equals("Download speed (low to high)")){
            deviceTiles.sortTilesByDownloadSpeed(true);
        }
        else if (selectedSortName.equals("Upload speed (high to low)")){
            deviceTiles.sortTilesByUploadSpeed(false);
        }
        else if (selectedSortName.equals("Upload speed (low to high)")){
            deviceTiles.sortTilesByUploadSpeed(true);
        }
        else if (selectedSortName.equals("Last connected")){
            deviceTiles.sortTilesByLastConnected();
        }
        else if (selectedSortName.equals("First connected")){
            deviceTiles.sortTilesByFirstConnected();
        }
        else if (selectedSortName.equals("Signal strength (good to bad)")){
            deviceTiles.sortTilesBySignalStrength(false);
        }
        else if (selectedSortName.equals("Signal strength (bad to good)")){
            deviceTiles.sortTilesBySignalStrength(true);
        }
        else{
            Log.e("SORT TILES", "sort option not found");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}