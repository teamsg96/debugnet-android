package com.debugnet.activities;

import androidx.appcompat.app.AppCompatActivity;
import rest.debugnet_rest_service.DebugnetAPI;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.debugnet.R;
import com.debugnet.activities.common.DialogGenerator;
import com.debugnet.activities.common.GoToSettingsCallback;

public class SelectFactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_fact);
    }

    /*
    Check which query option was selected and move onto the next activity
    */
    public void onClick(View v) {
        DebugnetAPI.FACTUAL_QUERY query = null;

        Intent intent = new Intent(SelectFactActivity.this,
                FactualResponseActivity.class);

        // Check which query option was selected and store its identifier as denoted by the
        // Debugnet API.
        switch (v.getId()) {
            case R.id.internetSpeedButton:
                query = DebugnetAPI.FACTUAL_QUERY.FACTUAL_CURR_INTERNET_SPEED;
                break;

            case R.id.internetBestWorstTimeButton:
                query = DebugnetAPI.FACTUAL_QUERY.FACTUAL_INTERNET_FAST_AND_SLOW_TOD;
                break;

            case R.id.ispDeliveryButton:
                query = DebugnetAPI.FACTUAL_QUERY.FACTUAL_ISP_PERFORMANCE;

                SharedPreferences sP = getSharedPreferences("settings", MODE_PRIVATE);

                // Display an info popup if either the promised upload speed or download speed is
                // not set. Both are required to perform the operation. The popup will allow the
                // user to navigate to settings.
                if (!sP.contains("promisedUploadSpeed") || !sP.contains("promisedDownloadSpeed")){
                    String title = "Promised ";
                    String msg = "To proceed this must first be specified under settings";

                    // Be specific in title about what isn't set
                    if (!sP.contains("promisedUploadSpeed")){
                        title += "upload speed ";
                    }

                    if (!sP.contains("promisedUploadSpeed") && !sP.contains("promisedDownloadSpeed")){
                        title += "and ";
                    }

                    if (!sP.contains("promisedDownloadSpeed")){
                        title += "download speed ";
                    }

                    title += "not set";

                    DialogGenerator.createInfoBoxWithCustomButton(
                            title,
                            msg,
                            "Take me to settings",
                            new GoToSettingsCallback(this),
                            this);

                    return;
                }

                // Store the promised speeds inside the intent
                intent.putExtra("promisedUploadSpeed",
                        sP.getFloat("promisedUploadSpeed", -1));

                intent.putExtra("promisedDownloadSpeed",
                        sP.getFloat("promisedDownloadSpeed", -1));

                break;

            default:
                return;
        }

        // Move onto the next activity if one of the query buttons was selected
        intent.putExtra("queryId", query);
        startActivity(intent);
    }

}
