package com.debugnet.activities.common;

import androidx.appcompat.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/*
Generates dialog boxes for DebugNet
 */
public class DialogGenerator {
    /*
    Create a error dialog with a message for when a problem occurs communicating with the router
    */
    static public AlertDialog createErrorBoxForCommunicationError(
            DialogInterface.OnClickListener okListener,
            Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("Ok", okListener);

        AlertDialog errorBox = builder.create();
        errorBox.setTitle("Error whilst communicating with the router");
        errorBox.setMessage("Try again later and try restarting the router");
        errorBox.show();

        return errorBox;
    }

    /*
    Create a error dialog with a message for when a connection cannot be made to the router.
    */
    static public AlertDialog createErrorBoxForNoConnection(DialogInterface.OnClickListener okListener,
                                                            Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("Ok", okListener);

        AlertDialog errorBox = builder.create();
        errorBox.setTitle("Unable to connect to the router");
        errorBox.setMessage("Check your device is connected to the same network as the DebugNet supported router");
        errorBox.show();

        return errorBox;
    }

    /*
    Create a popup box informing the user to wait for communication with the router to finish. Once
    complete the box will close.
     */
    static public AlertDialog createInfoBoxForCommunicationWait(Context context){
        return createInfoBoxNoButton(
                "Communicating with the router...",
                "",
                context
        );
    }

    /*
    Create a info dialog with a message. Upon clicking 'Ok' the box will simply close.
    */
    static public AlertDialog createInfoBoxOverlaid(String title, String msg, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Clicking ok will return the user to the screen the popup box is on top off.
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog infoBox = builder.create();
        infoBox.setTitle(title);
        infoBox.setMessage(msg);
        infoBox.show();

        return infoBox;
    }

    /*
    Create a popup box to show information. A listener is given for when 'Ok' is pressed.
    */
    static public AlertDialog createInfoBoxWithOkButton(String title,
                                                        String msg,
                                                        DialogInterface.OnClickListener okListener,
                                                        Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("Ok", okListener);
        AlertDialog loadingBox = builder.create();
        loadingBox.setTitle(title);
        loadingBox.setMessage(msg);
        loadingBox.setCancelable(false);
        loadingBox.show();

        return loadingBox;
    }

    /*
    Create a popup box to show information. The box has no buttons and cant be canceled by the user
     */
    static public AlertDialog createInfoBoxNoButton(String title, String msg, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog loadingBox = builder.create();
        loadingBox.setTitle(title);
        loadingBox.setMessage(msg);
        loadingBox.setCancelable(false);
        loadingBox.show();

        return loadingBox;
    }

    /*
    Create a popup box to show information. A listener is given for when the positive button is
    pressed.
    */
    static public AlertDialog createInfoBoxWithCustomButton(
            String title,
            String msg,
            String buttonMsg,
            DialogInterface.OnClickListener buttonListener,
            Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(buttonMsg, buttonListener);
        AlertDialog loadingBox = builder.create();
        loadingBox.setTitle(title);
        loadingBox.setMessage(msg);
        loadingBox.show();

        return loadingBox;
    }
}
