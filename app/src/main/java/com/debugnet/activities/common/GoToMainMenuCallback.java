package com.debugnet.activities.common;

import android.content.DialogInterface;
import android.content.Intent;

import com.debugnet.activities.MainActivity;

import androidx.appcompat.app.AppCompatActivity;

/*
Listener to use when a click of an item should return to the main menu
 */
public class GoToMainMenuCallback implements DialogInterface.OnClickListener{
    private AppCompatActivity currentActivity;

    public GoToMainMenuCallback(AppCompatActivity activity) {
        this.currentActivity = activity;
    }

    @Override
    public void onClick(DialogInterface dialog, int which){
        currentActivity.startActivity(new Intent(currentActivity, MainActivity.class));
    }
}
