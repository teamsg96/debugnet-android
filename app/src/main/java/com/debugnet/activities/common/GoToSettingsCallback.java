package com.debugnet.activities.common;

import android.content.DialogInterface;
import android.content.Intent;

import com.debugnet.activities.SettingsActivity;

import androidx.appcompat.app.AppCompatActivity;

/*
Listener to use when a click of an item should navigate to the main menu
 */
public class GoToSettingsCallback implements DialogInterface.OnClickListener{
    private AppCompatActivity currentActivity;

    public GoToSettingsCallback(AppCompatActivity activity) {
        this.currentActivity = activity;
    }

    @Override
    public void onClick(DialogInterface dialog, int which){
        currentActivity.startActivity(new Intent(currentActivity, SettingsActivity.class));
    }
}
