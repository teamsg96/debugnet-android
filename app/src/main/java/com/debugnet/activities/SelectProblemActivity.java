package com.debugnet.activities;

import androidx.appcompat.app.AppCompatActivity;
import rest.debugnet_rest_service.DebugnetAPI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.debugnet.R;

public class SelectProblemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_problem);
    }

    /*
    Check which query option was selected and move onto the next activity
     */
    public void onClick(View v) {
        DebugnetAPI.TROUBLESHOOT_QUERY query = null;

        // Check which query option was selected and store its identifier as denoted by the
        // Debugnet API.
        switch (v.getId()) {
            case R.id.tsMenuSlowDeviceInternetButton:
                query = DebugnetAPI.TROUBLESHOOT_QUERY.TROUBLESHOOT_SLOW_DEVICE;
                break;

            case R.id.tsMenuConnDropButton:
                query = DebugnetAPI.TROUBLESHOOT_QUERY.TROUBLESHOOT_CONN_DROP;
                break;

            case R.id.tsMenuSlowNetworkButton:
                query = DebugnetAPI.TROUBLESHOOT_QUERY.TROUBLESHOOT_SLOW_NETWORK;
                break;
        }

        // Move onto the next activity if one of the query buttons was selected
        if (query != null){
            Intent intent = new Intent(SelectProblemActivity.this,
                                        AdditionalTsOptsActivity.class);
            intent.putExtra("queryId", query);
            startActivity(intent);
        }
    }

}
