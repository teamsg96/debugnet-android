package com.debugnet.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import rest.debugnet_rest_service.DebugnetAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.debugnet.R;
import com.debugnet.activities.common.DialogGenerator;
import com.debugnet.activities.common.GoToMainMenuCallback;

public class FactualResponseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factual_response);

        // Make both text view boxes scrollable
        TextView queryNameBox = findViewById(R.id.factualQueryNameBox);
        queryNameBox.setMovementMethod(new ScrollingMovementMethod());
        TextView responseBox = findViewById(R.id.factualResponseBox);
        responseBox.setMovementMethod(new ScrollingMovementMethod());

        // Hide all content for now until the troubleshoot report has been returned
        ConstraintLayout factualResponseLayout = findViewById(R.id.factualResponseLayout);
        factualResponseLayout.setVisibility(View.INVISIBLE);

        // Display a popup to tell the user to wait whilst the router is communicated with.
        AlertDialog loadingPopup = DialogGenerator.createInfoBoxForCommunicationWait(this);

        // Execute factual query request and listen out for a response with a callback
        new FactualQueryHandler(this, loadingPopup).executeFactualQuery(
                (DebugnetAPI.FACTUAL_QUERY) getIntent().getSerializableExtra("queryId"));
    }

    /*
    Go to the main menu if the user presses done
    */
    public void onDoneClick(View v){
        startActivity(new Intent(this, MainActivity.class));
    }
}

/*
Handles the execution of a factual query on the frontend
 */
class FactualQueryHandler implements Callback<String> {
    private AlertDialog loadingPopup;
    private FactualResponseActivity activity;

    public FactualQueryHandler(FactualResponseActivity activity, AlertDialog loadingPopup) {
        this.activity = activity;
        this.loadingPopup = loadingPopup;
    }

    /*
    Execute the desired factual query and execute it with the required arguments
     */
    public void executeFactualQuery(DebugnetAPI.FACTUAL_QUERY queryId) {
        TextView queryNameBox = activity.findViewById(R.id.factualQueryNameBox);
        String queryName = "";
        DebugnetAPI dbNetApi = new DebugnetAPI();
        dbNetApi.setup();

        Intent intent = activity.getIntent();

        switch (queryId) {
            case FACTUAL_CURR_INTERNET_SPEED:
                queryName = "What is the current internet speed from the router to my internet service provider?";
                dbNetApi.runGetCurrentInternetSpeed(this);
                break;

            case FACTUAL_INTERNET_FAST_AND_SLOW_TOD:
                queryName = "What times of the day is the internet speed the most quickest and most slowest?";
                dbNetApi.runGetTODInternetIsFastAndSlow(this);
                break;

            case FACTUAL_ISP_PERFORMANCE:
                queryName = "Internet service provider performance report";
                dbNetApi.runGetIspPerformance(
                        intent.getFloatExtra("promisedUploadSpeed", -1),
                        intent.getFloatExtra("promisedDownloadSpeed", -1),
                        this);
                break;
        }

        queryNameBox.setText(queryName);

    }

    /*
    Handle the response from a factual query request
     */
    public void onResponse(Call<String> call, Response<String> response) {
        Log.i("REST", "Code: " + String.valueOf(response.code()));

        loadingPopup.cancel();

        if (!response.isSuccessful()) {
            DialogGenerator.createErrorBoxForCommunicationError(
                    new GoToMainMenuCallback(activity),
                    activity
            );
            return;
        }

        // Make the report window visible
        activity.findViewById(R.id.factualResponseLayout).setVisibility(View.VISIBLE);

        // Display the response
        TextView factualResponseBox = activity.findViewById(R.id.factualResponseBox);
        factualResponseBox.setText(Html.fromHtml(response.body()));
    }

    /*
    Handle when the REST call fails.
     */
    public void onFailure(Call<String> call, Throwable throwable) {
        DialogGenerator.createErrorBoxForNoConnection(
                new GoToMainMenuCallback(activity),
                activity
        );

        Log.e("REST", throwable.toString());
    }
}
