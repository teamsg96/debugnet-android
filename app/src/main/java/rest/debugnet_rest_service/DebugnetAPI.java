package rest.debugnet_rest_service;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import rest.debugnet_rest_service.objs.Device;
import rest.debugnet_rest_service.objs.TroubleshootResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DebugnetAPI {
    private String baseUrl = "http://192.168.8.1:9080/";
    private DebugnetRestAPIInterface dbNetService = null;

    private final int TIMEOUT = 30;

    // Identifier for each troubleshoot query option
    public enum TROUBLESHOOT_QUERY{
        TROUBLESHOOT_CONN_DROP,
        TROUBLESHOOT_SLOW_DEVICE,
        TROUBLESHOOT_SLOW_NETWORK
    }

    // Identifier for each factual query option
    public enum FACTUAL_QUERY{
        FACTUAL_CURR_INTERNET_SPEED,
        FACTUAL_INTERNET_FAST_AND_SLOW_TOD,
        FACTUAL_ISP_PERFORMANCE,
    }

    /*
    Prepare for call to DebugnetRestAPI
     */
    public void setup(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient).build();
        dbNetService = retrofit.create(DebugnetRestAPIInterface.class);
    }

    /*
    Request troubleshoot on a slow device
     */
    public void runSlowDeviceTroubleshoot(String deviceMac, int slowTime,
                                          Callback<TroubleshootResponse> callback){
        Call<TroubleshootResponse> call = dbNetService.runSlowDeviceTroubleshoot(
                deviceMac, slowTime);
        call.enqueue(callback);
    }

    /*
    Request troubleshoot on a device that regularly loses connection
     */
    public void runRegularConnDropDeviceTroubleshoot(String deviceMac, int instableTime,
                                                     Callback<TroubleshootResponse> callback){
        Call<TroubleshootResponse> call = dbNetService.runRegularConnDropDeviceTroubleshoot(
                deviceMac, instableTime);
        call.enqueue(callback);
    }

    /*
    Request troubleshoot on a slow network.
    */
    public void runSlowNetworkTroubleshoot(int slowTime, Callback<TroubleshootResponse> callback){
        Call<TroubleshootResponse> call = dbNetService.runSlowNetworkTroubleshoot(slowTime);
        call.enqueue(callback);
    }

    /*
    Request information about the current internet speed
    */
    public void runGetCurrentInternetSpeed(Callback<String> callback){
        Call<String> call = dbNetService.runGetCurrentInternetSpeed();
        call.enqueue(callback);
    }

    /*
    Request information about the time of day at which the internet is at its fastest and slowest
    */
    public void runGetTODInternetIsFastAndSlow(Callback<String> callback){
        Call<String> call = dbNetService.runGetTODInternetIsFastAndSlow();
        call.enqueue(callback);
    }

    /*
    Request information about the ISP performance
    */
    public void runGetIspPerformance(double expectedUp, double expectedDown, Callback<String> callback){
        Call<String> call = dbNetService.runGetIspPerformance(expectedUp, expectedDown);
        call.enqueue(callback);
    }

    /*
    Request all connected devices on the router.
    */
    public void getDevices(Callback<ArrayList<Device>> callback){
        Call<ArrayList<Device>> call = dbNetService.getDevices();
        call.enqueue(callback);
    }

    /*
    Request all connected devices on the router.
    */
    public void getDevicesWithMetrics(int secondsToMeasure, Callback<ArrayList<Device>> callback){
        Call<ArrayList<Device>> call = dbNetService.getDevicesWithMetrics(secondsToMeasure);
        call.enqueue(callback);
    }
}
