package rest.debugnet_rest_service.objs;

import com.google.gson.annotations.SerializedName;

/*
Holds Information about a detected problem
 */
public class Problem {
    @SerializedName("problem") private String problemDescription;
    @SerializedName("advice") private String advice;

    public Problem(String problemDescription, String advice) {
        this.problemDescription = problemDescription;
        this.advice = advice;
    }

    /*
    Returns the nature of the problem
     */
    public String getProblemDescription() {
        return problemDescription;
    }

    /*
    Returns advice to help solve the problem
     */
    public String getAdvice() {
        return advice;
    }
}
