package rest.debugnet_rest_service.objs;

import com.google.gson.annotations.SerializedName;

public class Device {
    @SerializedName("mac") private String mac;
    @SerializedName("human_name") private String humanName;
    @SerializedName("status") private int status;
    @SerializedName("conn_type") private int connType;
    @SerializedName("first_connected") private int firstConnected;
    @SerializedName("last_connected") private int lastConnected;
    @SerializedName("hostname") private String hostname;
    @SerializedName("ip_address") private String ipAddress;
    @SerializedName("manufacturer") private String manufacturer;
    @SerializedName("download_speed") private double downloadSpeed;
    @SerializedName("upload_speed") private double uploadSpeed;
    @SerializedName("signal_strength") private double signalStrength;

    public enum CONN_TYPE{
        CONN_OTHER,
        CONN_NONE,
        CONN_WIFI_2_4,
        CONN_WIFI_5
    }

    public enum DEV_STATUS{
        DEV_ONLINE,
        DEV_OFFLINE
    }

    public Device(String mac, String humanName, int status, int connType, int firstConnected,
                  int lastConnected, String hostname, String ipAddress, String manufacturer,
                  double downloadSpeed, double uploadSpeed, double signalStrength) {
        this.mac = mac;
        this.humanName = humanName;
        this.status = status;
        this.connType = connType;
        this.firstConnected = firstConnected;
        this.lastConnected = lastConnected;
        this.hostname = hostname;
        this.ipAddress = ipAddress;
        this.manufacturer = manufacturer;
        this.downloadSpeed = downloadSpeed;
        this.uploadSpeed = uploadSpeed;
        this.signalStrength = signalStrength;
    }

    /*
    Get MAC address of device
    */
    public String getMac() {
        return mac;
    }

    /*
    Get MAC address with no colons
     */
    public String getMacNoColons() {
        return mac.replace(":", "");
    }

    /*
    Get human name of device
     */
    public String getHumanName() {
        return humanName;
    }

    /*
    Get the status of the device
     */
    public DEV_STATUS getStatus() {
        // Convert and return status ID as enum
        switch (status){
            case (1):
                return DEV_STATUS.DEV_OFFLINE;

            case (2):
                return DEV_STATUS.DEV_ONLINE;

            default:
                return null;
        }
    }

    /*
    Get the status of the device as a string
    */
    public String getStatusStr(){
        // Convert and return status ID as string
        switch (getStatus()){
            case DEV_OFFLINE:
                return "Offline";

            case DEV_ONLINE:
                return "Online";

            default:
                return null;
        }
    }

    /*
    Get the type of connection
    */
    public CONN_TYPE getConnType() {
        // Convert and return connection type ID as enum
        switch (connType){
            case (1):
                return CONN_TYPE.CONN_OTHER;

            case (2):
                return CONN_TYPE.CONN_NONE;

            case (3):
                return CONN_TYPE.CONN_WIFI_2_4;

            case (4):
                return CONN_TYPE.CONN_WIFI_5;

            default:
                return null;
        }
    }

    /*
    Get the time the device was first connected
     */
    public int getFirstConnected() {
        return firstConnected;
    }

    /*
    Get the time the device was last connected
    */
    public int getLastConnected() {
        return lastConnected;
    }

    /*
    Get the device hostname
     */
    public String getHostname() {
        return hostname;
    }

    /*
    Get the IP address of the device
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /*
    Get the manufacturer
    */
    public String getManufacturer() {
        return manufacturer;
    }

    /*
    Get the download speed
     */
    public double getDownloadSpeed() {
        return downloadSpeed;
    }

    /*
    get the upload speed
     */
    public double getUploadSpeed() {
        return uploadSpeed;
    }

    /*
    Get wireless signal strength
     */
    public double getSignalStrength() {
        return signalStrength;
    }

    /*
        Checks if the device is connected by wireless
         */
    public boolean isWireless(){
        return getConnType() == CONN_TYPE.CONN_WIFI_2_4 || getConnType() == CONN_TYPE.CONN_WIFI_5;
    }
}
