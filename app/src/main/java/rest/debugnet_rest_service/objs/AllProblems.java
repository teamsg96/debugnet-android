package rest.debugnet_rest_service.objs;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
Holds all problems detected as a result of performing a troubleshoot
 */
public class AllProblems {
    @SerializedName("severe") private ArrayList<Problem> severeProblems;
    @SerializedName("moderate") private ArrayList<Problem> moderateProblems;

    public AllProblems(ArrayList<Problem> severeProblems, ArrayList<Problem> moderateProblems) {
        this.severeProblems = severeProblems;
        this.moderateProblems = moderateProblems;
    }

    /*
    Get all detected problems that are ranked as 'severe severity'
    */
    public ArrayList<Problem> getSevereProblems() {
        return severeProblems;
    }

    /*
    Get all detected problems that are ranked as 'moderate severity'
     */
    public ArrayList<Problem> getModerateProblems() {
        return moderateProblems;
    }

    /*
    Return all detected problems in the order of severity
     */
    public ArrayList<Problem> getAllProblemsInSeverityOrder(){
        ArrayList<Problem> allP = new ArrayList();
        allP.addAll(getSevereProblems());
        allP.addAll(getModerateProblems());

        return allP;
    }
}
