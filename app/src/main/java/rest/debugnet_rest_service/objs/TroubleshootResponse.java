package rest.debugnet_rest_service.objs;

import com.google.gson.annotations.SerializedName;

/*
Holds the response from performing a troubleshoot
 */
public class TroubleshootResponse {
    @SerializedName("query_name") private String queryName;
    @SerializedName("problems") private AllProblems allProblems;

    public TroubleshootResponse(String queryName, AllProblems allProblems) {
        this.queryName = queryName;
        this.allProblems = allProblems;
    }

    /*
    Get the name of the troubleshoot query
     */
    public String getQueryName() {
        return queryName;
    }

    /*
    Return all the problems detected
     */
    public AllProblems getAllProblems() {
        return allProblems;
    }
}
