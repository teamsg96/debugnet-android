package rest.debugnet_rest_service;

import java.util.ArrayList;

import rest.debugnet_rest_service.objs.Device;
import rest.debugnet_rest_service.objs.TroubleshootResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/*
Defines the REST client interface to DebugNetAPI
 */
public interface DebugnetRestAPIInterface {
    @GET("query/troubleshoot/slow-device/{device_mac}/{slow_time}")
    Call<TroubleshootResponse> runSlowDeviceTroubleshoot(
            @Path("device_mac") String deviceMac,
            @Path("slow_time") int slowTime);

    @GET("query/troubleshoot/conn-drop/{device_mac}/{instable_time}")
    Call<TroubleshootResponse> runRegularConnDropDeviceTroubleshoot(
            @Path("device_mac") String deviceMac,
            @Path("instable_time") int instableTime);

    @GET("query/troubleshoot/slow-network/{slow_time}")
    Call<TroubleshootResponse> runSlowNetworkTroubleshoot(
            @Path("slow_time") int slowTime);

    @GET("query/factual/current-internet-speed")
    Call<String> runGetCurrentInternetSpeed();

    @GET("query/factual/tod-internet-slowest-fastest")
    Call<String> runGetTODInternetIsFastAndSlow();

    @GET("query/factual/isp-performance/{expected_up}/{expected_down}")
    Call<String> runGetIspPerformance(
            @Path("expected_up") double expectedUp,
            @Path("expected_down") double expectedDown);

    @GET("devices")
    Call<ArrayList<Device>> getDevices();

    @GET("devices/{measure_time}")
    Call<ArrayList<Device>> getDevicesWithMetrics(
            @Path("measure_time") double measureTime);
}
